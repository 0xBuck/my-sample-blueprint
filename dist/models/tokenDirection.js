"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenDirection = void 0;
var TokenDirection;
(function (TokenDirection) {
    TokenDirection[TokenDirection["INPUT_TOKEN"] = 0] = "INPUT_TOKEN";
    TokenDirection[TokenDirection["OUTPUT_TOKEN"] = 1] = "OUTPUT_TOKEN";
})(TokenDirection = exports.TokenDirection || (exports.TokenDirection = {}));
