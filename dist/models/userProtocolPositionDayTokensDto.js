"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserProtocolPositionDayTokensDto = void 0;
class UserProtocolPositionDayTokensDto {
    constructor(dayDataToken) {
        this.tokenName = dayDataToken.tokenName;
        this.tokenAddress = dayDataToken.tokenAddress;
        this.netTokenAmount = dayDataToken.netTokenAmount;
        this.priceUsd = dayDataToken.priceUsd;
        this.priceSource = dayDataToken.priceSource;
        this.pendingIncomeAmount = dayDataToken.pendingIncomeAmount;
        // this.collectedIncomeAmount = dayDataToken.collectedIncomeAmount;
        // this.totalIncomeAmount = dayDataToken.totalIncomeAmount;
        // this.dailyIncomeAmount = dayDataToken.dailyIncomeAmount;
        // this.tokenAmountIfExit = dayDataToken.tokenAmountIfExit;
        // this.ifHeldAmountToken = dayDataToken.ifHeldAmountToken;
        // this.exitedIfHeldAmountToken = dayDataToken.exitedIfHeldAmountToken;
        // this.ifHeldAllAmountToken = dayDataToken.ifHeldAllAmountToken;
        // this.ifHeldAllAmountTokenValueUsd = dayDataToken.ifHeldAllAmountTokenValueUsd;
        // this.exitedTokenAmount = dayDataToken.exitedTokenAmount;
    }
}
exports.UserProtocolPositionDayTokensDto = UserProtocolPositionDayTokensDto;
