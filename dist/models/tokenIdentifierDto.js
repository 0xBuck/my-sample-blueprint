"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenIdentifierDto = void 0;
class TokenIdentifierDto {
    constructor(name, address) {
        this.name = name;
        this.address = address;
    }
}
exports.TokenIdentifierDto = TokenIdentifierDto;
