"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenInfo = void 0;
class TokenInfo {
    constructor(identifier, // token address | pool id (masterchef type)
    priceUsd, amount, // amount divided by token decimals
    source) {
        this.identifier = identifier;
        this.priceUsd = priceUsd;
        this.amount = amount;
        this.source = source;
    }
}
exports.TokenInfo = TokenInfo;
