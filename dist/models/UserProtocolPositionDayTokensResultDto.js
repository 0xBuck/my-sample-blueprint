"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserProtocolPositionDayTokensResultDto = void 0;
class UserProtocolPositionDayTokensResultDto {
    constructor(tokenName, tokenAddress, netTokenAmount, priceUsd, priceSource, pendingIncomeAmount, collectedIncomeAmount, totalIncomeAmount, dailyIncomeAmount, tokenAmountIfExit, ifHeldAmountToken, ifHeldAllAmountToken, ifHeldAllAmountTokenValueUsd) {
        this.tokenName = tokenName;
        this.tokenAddress = tokenAddress;
        this.netTokenAmount = netTokenAmount;
        this.priceUsd = priceUsd;
        this.priceSource = priceSource;
        this.pendingIncomeAmount = pendingIncomeAmount;
        this.collectedIncomeAmount = collectedIncomeAmount;
        this.totalIncomeAmount = totalIncomeAmount;
        this.dailyIncomeAmount = dailyIncomeAmount;
        this.tokenAmountIfExit = tokenAmountIfExit;
        this.ifHeldAmountToken = ifHeldAmountToken;
        this.ifHeldAllAmountToken = ifHeldAllAmountToken;
        this.ifHeldAllAmountTokenValueUsd = ifHeldAllAmountTokenValueUsd;
    }
}
exports.UserProtocolPositionDayTokensResultDto = UserProtocolPositionDayTokensResultDto;
